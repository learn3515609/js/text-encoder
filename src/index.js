const string = '_Frontend_';

const decoder = new TextDecoder();
const encoder = new TextEncoder();

const data1 = encoder.encode(string).subarray(1, -1);
const result1 = decoder.decode(data1);

console.log('result 1', result1);

const data2 = new Uint8Array(string.length);
encoder.encodeInto(string, data2);
const result2 = decoder.decode(data2);

console.log('result 2', result2);